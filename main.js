const app = new Vue({
  el: "#app",
  data: {
    showModal: false,
    tweets: ["This is second tweet", "This is first tweet"],
    newTweet: "",
    maxChars: 140,
    loading: false
  },
  computed: {
    charsLeft() {
      return this.maxChars - this.newTweet.length;
    }
  },
  methods: {
    show() {
      this.showModal = !this.showModal;
      this.newTweet = "    ";
    },
    addTweet() {
      this.tweets.unshift(this.newTweet);
      this.newTweet = "";

      this.loading = true;
      setTimeout(() => {
        this.loading = false;
        this.showModal = false;
      }, 2000);
    },
    deleteTweet(tweet) {
      const index = this.tweets.indexOf(tweet);
      this.tweets.splice(index, 1);
    }
  }
});
